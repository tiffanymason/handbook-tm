module Milestones
  # NOTE: Future iterations we will replace this method with methods that retrieve data from the API
  def milestone_to_date(milestone)
    milestones = {
      '14.3' => "September 22, 2021",
      '14.4' => "October 22, 2021",
      '14.5' => "November 22, 2021",
      '14.6' => "December 22, 2021",
      '14.7' => "January 22, 2022",
      '14.8' => "February 22, 2022",
      '14.9' => "March 22, 2022",
      '14.10' => "April 22, 2022",
      '15.0' => "May 22, 2022"
    }
    milestones.fetch(milestone, "unknown")
  end
end
