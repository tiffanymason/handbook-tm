---
layout: handbook-page-toc
title: "Reliability Engineering"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

If you are a GitLab team member and are looking to alert Reliability Engineering about an availability issue with GitLab.com, please find quick instructions to report an incident here: [Reporting an Incident](/handbook/engineering/infrastructure/incident-management/#reporting-an-incident).
{: .alert .alert-danger}

If you are a GitLab team member looking for assistance from Reliability Engineering, please see the [Getting Assistance](#getting-assistance) section.
{: .alert .alert-info}


## Who We Are

Reliability Engineering is responsible for all of GitLab's user-facing services, with their primary responsibility being GitLab.com. Site Reliability Engineers (SREs) ensure the availability of these services, building the tools and automation to monitor and enable this availability. These user-facing services include a multitude of environments, including staging, GitLab.com, and dev.GitLab.org, among others (see the [list of environments](/handbook/engineering/infrastructure/environments/)).


## Vision

**Reliability Engineering** ensures that GitLab's customers can rely on GitLab.com for their mission-critical workloads. We approach availability as an engineering challenge and empower our counterparts in Development to make the best possible infrastructure decisions. We own and iterate often on [how we manage incidents](/handbook/engineering/infrastructure/incident-management/) and continually derive and share our learnings by conducting [thorough reviews of those incidents](/handbook/engineering/infrastructure/incident-review/).


## Tenets
1. [**Change Management**](/handbook/engineering/infrastructure/change-management/), [**Incident Management**](/handbook/engineering/infrastructure/incident-management/), [**Incident Review**](/handbook/engineering/infrastructure/incident-review/) and [**Delta Management**](/handbook/engineering/infrastructure/library/production/deltas/) are owned by Reliability Engineering.
1. Each team member is able to work on all team projects.
1. The team is able to reach conclusions independently all the time, consensus most of the time.
1. Career development paths are clear.
1. The team maintains a database of SRE knowledge through documentation, training sessions, and outreach.
1. We leverage the GitLab product where we can in our toolchain.


## Team

The Reliability Engineering team is composed of [DBRE](/job-families/engineering/infrastructure/database-reliability-engineer/)s and [SRE](/job-families/engineering/infrastructure/site-reliability-engineer/)s. As the role titles indicate, they have different areas of specialty but shared ownership of GitLab.com's availability. The team is broken down into three sub-teams, each with their own area of ownership.

#### Core Infra

[Team Page](/handbook/engineering/infrastructure/team/reliability/core-infra/)

The Core Infra teams owns core infrastructure tooling, network ingress/egress, CDNs, DNS, and secrets management.

<%= partial "handbook/engineering/infrastructure/team/reliability/core-infra/_core_infra_team.html" %>

#### Datastores

[Team Page](/handbook/engineering/infrastructure/team/reliability/datastores/)

The Datastores team owns our persistent storage platforms, with PostgreSQL on gitlab.com being the top priority.

Datastores is:

| Person | Role |
| ------ | ------ |
|Open Position|[Engineering Manager, Reliability](https://about.gitlab.com/job-families/engineering/infrastructure/engineering-management/#engineering-manager-reliability)|
|[Alejandro Rodríguez](/company/team/#eReGeBe)|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Ahmad Sherif](/company/team/#ahmadsherif)|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Jose Cores Finotto](/company/team/#jose-finotto)|[Staff Database Reliability Engineer](/job-families/engineering/infrastructure/database-reliability-engineer/)|
|[Nels Nelson](/company/team/#nnelson)|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|Open Position|[Database Reliability Engineer](/job-families/engineering/infrastructure/database-reliability-engineer/)|
|Open Position|[Database Reliability Engineer](/job-families/engineering/infrastructure/database-reliability-engineer/)|


#### Observability

[Team Page](/handbook/engineering/infrastructure/team/reliability/observability/)

The Observability team owns the monitoring and alerting infrastructure for GitLab.com, as well as our caching/queuing infrastructure.

Observability is:

| Person | Role |
| ------ | ------ |
|[Kennedy Wanyangu](/company/team/#kwanyangu)|[Engineering Manager, Reliability](/job-families/engineering/infrastructure/engineering-management/#engineering-manager-reliability)|
|[Igor Wiedler](/company/team/#igorwwwwwwwwwwwwwwwwwwww)|[Staff Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Michal Wasilewski](/company/team/#mwasilewski-gitlab)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Natan Hoppe](/company/team/#nhoppe1)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Rehab Hassanein](/company/team/#rehab)|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Anthony Maina](/company/team/#anganga)|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Steve Azzopardi](/company/team/#steveazz)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|Open Position|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|Open Position|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|


## How We Work

Though we are organized around teams focused on specific domains, the workflow for Reliability remains the same across Reliability teams. The reasoning behind this is to allow for team members to readily engage in projects where they have expertise, but which are related to domains maintained by another Reliability team. An example here would be an SRE from the Datastores team adding their Ansible expertise to a project with a DRI in Core Infra.

As Reliability, we have three main work streams and each varies in the type of work. Those three work streams are:
1. [Projects](#how-we-work--projects) - These are pre-planned units of work, managed via an epic and a single DRI who collaborates with other members of Reliability.
1. [EOC Queue](#how-we-work--eoc-queue) - This is a quarterly milestone consisting of corrective actions and EOC tooling maintenance.
1. [Customer Request Queue](#how-we-work--customer-request-queue) - This is a quarterly milestone of all small-scale asks which originate from teams outside of Reliability and which are not tied a Project of corrective action. Examples here include adding DNS entries, provisioning/de-provisioning system access, and assisting Support.


### How We Work -- Projects

We maintain a single source of truth epic for all work underway for the Reliability team. That epic can be found at [GitLab SaaS Reliability - work queue](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/509) and represents the current state of project work for the team. That epic references projects detailed in the form of sub-epics.

#### Project Squads
Projects are assigned and handled within squads. A squad is composed of a minimum of 2 SREs who are responsible for all the sub-epics and issues within a project. 
Reliability engineering managers are responsible for the formation of squads and the assignment of projects to the respective squads. 

The squad works in a collaborative, transparent approach and uses the strengths of respective SRE to deliver the project in the highest quality within the least amount of time.
Responsibilities of squads
1. Appoint a DRI between themselves
1. Scope with timelines and deliver on the issues within the assigned epic
1. Assign individual issues within an epic to individual squad members
1. Hold weekly recorded demos, where possible as per the project needs
1. DRI - Keep the status of the project updated as frequently as necessary, minimum weekly
1. Have a balance of experience and subject matter skills

#### Roles
Project DRI - Leads the squad to make hard project decisions and act on them immediately.
Engineering Manager -  Facilitate communication(upfront context) and strong alignment between squads

Project epics use the following [template](#project-epic-template) in order to capture, current status, DRI, an overview, and any references that help a reader to build context.

Project epics also are labeled with the `team::Reliability` label and either `workflow-infra::Proposal` for projects proposed and not yet begun or `workflow-infra::In Progress` for projects which are underway.

Project epic descriptions have this structure:
1. Status - Current status of the project preceded by the ISO formatted date (YYYY-MM-DD) of the latest status update. Example: "2021-07-20 - DRI selected and initial project sync meeting held."
1. DRI - Directly responsible individual tasked with maintaining project status. This is the GitLab handle of the DRI, `@GitLabHandle`
1. Overview - Narrative description of the project intended to provide additional context. This is a few paragraphs setting context for someone new to the project and wanting to learn more and contribute.
1. Reference - Links out to existing artifacts which allow team members to build context. These are links to documents and any dependent projects or prior art.

##### Project Epic Template
```markdown
#### Status
2021-YY-XX - {+ Current status +}

***
#### DRI
{+ GitLab username of the DRI +}

***
#### Overview
{+ Overview of the project and its goals +}

***
#### Reference
1. {+ enumerated list of any reference links for a reader building context +}
```

example project epics:
1. [https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/471](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/471)

### How We Work -- EOC Queue

We maintain a single queue of corrective actions and work which the EOC on duty should fall back to when not actively engaged in incident management. We apply a quarterly milestone to issues in this queue and use a board based on the milestone to indicate priority and where work is at within the workflow.

- [EOC Queue Milestones](https://gitlab.com/groups/gitlab-com/gl-infra/-/milestones?search_title=EOC+Queue&state=&sort=)
- [EOC Queue Board](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/boards/2866217)

### How We Work -- Customer Request Queue

We maintain a single queue of active customer requests (asks from other departments are considered customer requests). Work here will be added via a recurring triage process of issues generated from outside the team.

- [Customer Request Queue Milestones](https://gitlab.com/groups/gitlab-com/gl-infra/-/milestones?search_title=customer+request+queue&state=&sort=)
- [Customer Request Queue Board](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/boards/2866210)


### How We Work -- Issue Triage

There is also a primary backlog for Reliability board which serves as a singular point of triage for work which has been generated externally to a specific Reliability team. That board is located at [Reliability Team - Backlog](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1688533?milestone_title=No+Milestone&&label_name[]=team%3A%3AReliability).

Issues in the Reliability team work streams come from several sources:

1. Issues generated by customers (teams outside Infrastructure) via one of the paths documented in the [Getting Assistance](#getting-assistance) section. These issues are automatically added to the triage backlog and are processed by Reliability managers.
1. Issues generated as [`Corrective Actions`](/handbook/engineering/infrastructure/incident-management/#sts=Corrective%20Actions) for incidents.
1. Issues generated as miscellaneous small tasks found in the day-to-day of an SRE/DBRE/EM

What the above all have in common is that they have two labels associated with them, `workflow-infra::Triage` and `team::Reliability`.

Issues in the [Reliability Team - Backlog](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1688533?milestone_title=No+Milestone&label_name[]=team%3A%3AReliability&group_by=epic) are moved from `workflow-infra::Triage` to the respective milestone or gathered into a project epic and placed in the `workflow-infra::Ready` state.


## Getting Assistance

If you're a GitLab team member and are looking to alert Reliability Engineering about an availability issue with GitLab.com, please find quick instructions to report an incident here: [Reporting an Incident](/handbook/engineering/infrastructure/incident-management/#reporting-an-incident).
{: .alert .alert-danger}

If you'd like our assistance, please use one of the issue generation templates below and the work will be routed appropriately:

* [Open a General Request Issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/new?issuable_template=request-general) - follow this link to create a general issue for Reliability Engineering.
* [Open a Customer Questions and Sales Enablement Issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/new?issuable_template=request-sales-enablement) - follow this link to seek assistance in answering questions for prospects or current customers.
* [Open a request for Infrastructure to join customer call](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/new?issuable_template=sre_join_customer_call_request).

We can also be reached in Slack in the [#production](https://gitlab.slack.com/archives/C101F3796) channel for questions related to GitLab.com and in the [#infrastructure-lounge](https://gitlab.slack.com/archives/CB3LSMEJV) channel for all other questions.
