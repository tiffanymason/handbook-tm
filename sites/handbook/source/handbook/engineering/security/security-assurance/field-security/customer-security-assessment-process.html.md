---
layout: handbook-page-toc
title: "GitLab's Customer Assurance Activities"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose
It's no surprise that GitLab Customers and Prospects conduct Security due diligence activities prior to contracting with GitLab. We recognize the importance of these reviews and have designed this procedure to document the intake, tracking and responses to these activities.

## Scope
The Customer Assurance Activities Procedure is applicable to Security due dilligence reviews requested by GitLab Customers and Prospects. This includes, but is not limited to, requests for Security Questionnaires, Contract Reviews, Customer Meetings, and External Evidence (such as a SOC2 or Penetration Test).

## Roles and Responsibilities

| Role | Responsibility |
| ------ | ------ |
| **Field Security team** | Maintain a mechanism to intake and respond to Customer Assurance Activities|
| | Provide complete and accurate responses within documented SLA|
| | Document and report any risk or trends identified during Customer Assurance Activities |
| | Maintain Metrics for [Security KPI](/handbook/engineering/security/performance-indicators/#security-impact-on-iacv)
| **Account Owner** | Recieve Customer or Prospect requests and submit to Field Security team |
| | Engage Solutions Architect or Technical Account Manager, as needed |
| | Provide information and documentation back to Customer or Prospect |
| **Solutions Architect** | Complete the First Pass, as applicable |
| **Current Customers** | Utilize Customer Self-Service tools noted below |
| |Contact your [Account Owner](/handbook/sales/#initial-account-owner---based-on-segment) at GitLab. If you don't know who that is, please [reach out to sales](https://about.gitlab.com/sales/) and ask to be connected to your Account Owner. |
| **Prospective Customers** | [Fill out a request](https://about.gitlab.com/sales/) and a representative will reach out to you. |


## Customer Self-Service Information Gathering

At GitLab, we are extremely [transparent](/handbook/values/#transparency) and the answers to many common questions may already be publicly available. However, we also [iterate](/handbook/values/#iteration) quickly, meaning the answers may change over time. We encourage our Customers and Prospects to utilize the below self-service resources as a first step in the process of assessing GitLab. 

* Search for [General Information about GitLab](https://about.gitlab.com) in our public handbook.
* Review [GitLab's Customer Assurance Package](https://about.gitlab.com/security/cap/)
* Review [GitLab's Product Security Documentation](https://docs.gitlab.com)
* Review [GitLab's SOC 3 Report](https://gitlab.com/gitlab-com/gl-security/soc-3-project)

In addition, GitLab team members have access to our **internal** database of commonly asked questions and answers, [**GitLab AnswerBase**](/handbook/engineering/security/security-assurance/field-security/answerbase.html). We ask that prior to submitting a new request, GitLab team members utilize this tool and answer any questions they can. If the answer can't be found in the **GitLab AnswerBase**, GitLab Team Members can:
   * Pose a new question in GitLab AnswerBase
   * Pose a new question in the [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) Slack Channel. 
   * Search for prior questions and answers in Slack using the following format: `[keywords] in:[channel name]`

**Self-attestations** 
<br>In the spirit of iteration, GitLab is continuously evolving our list of compliance self-attestations. Completed self-attestations are reviewed annually for continued applicability and can be found in our [Customer Assurance Package](https://about.gitlab.com/security/cap/). Customers can submit suggestions and requests for new self-attestations through their Account Manager. GitLab team members can submit recommendations for future compliance assessments through the [Regulatory Security Compliance Feedback and Field Research epic](https://gitlab.com/groups/gitlab-com/gl-security/-/epics/56).

Should these resources not provide you with sufficient information, your Account Owner will follow the GitLab Assisted Information Gathering Process below. 

## GitLab Assisted Information Gathering

Despite GitLab's extereme transparency, there may be items our Customers and Prospects can't find using our Self Service resources above. Account Owners will facilitate these requests using the procedure below. 

1. Using the Customer Assurance Activities workflow in the [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) Slack Channel, GitLab team members submit a request. The data requested in this workflow is **critical** for measuring our [Security KPI](/handbook/engineering/security/performance-indicators/#security-impact-on-iacv) and department goals. 
   * **Customer or Prospect Name**: The account name in SFDC
   * **Account Owner**
   * **Solutions Architect/Technical Account Manager** (if applicable)
   * **Request Type**: Select from: 
     * Security Questionnaire/Follow Up Questions
     * Contract Review
     * Customer Meeting
     * External Evidence (SOC, PCI, BCP, Penetration Test)
   * **Market or Indusrty Vertical**: the "industry" in the Account Details section of SFDC. These values tie dirctly to SiSence filters.
   * **Customer or Prospect Size**: the "sales segment" in the Sales Territory section of SFDC. These values tie dirctly to SiSence filters.
   * **Link to SFDC Opportunity**
   * **Product Host:** Select from:
     * SaaS
     * Self-Managed
     * Others 
   * **ARR Impact of the Opportunity**
   * **Due Date:**This is a free text box due to Slack restrictions. The date should be formatted as **YYYY-MM-DD** and inline with the 10 business day SLA. 
   * **Other:** Include any relevant information such as specific areas of concern, URL reference to a GitLab issue, URL to any google documents, or other relevant information to support the team. **Note**: All External Evidence requests, **MUST** include the Name and Email of the recipient. 

An automated work flow will take the data from the Slackbot and create an issue within [Field Security's GitLab Service Desk](https://gitlab.com/gitlab-com/gl-security/security-assurance/risk-field-security-team/customer-assurance-activities/caa-servicedesk/-/issues). Customer Assurance Activities are assigned and completed in the GitLab Service Desk. Updates will be done via the comments within those issues. 

## Service Level Agreements 
| CAA Request Type | SLA | Notes |
| -- | ----- | ----- |
| Security Questionnaire | 10 Business Days | SA or TAM will utlize AnswerBase and/or other self-service resources prior to requesting Field Security assistance.<br/>SA or TAM will ensure everyone on the Field Security team has access to any files or portals. |
| Contract Review | 5 Business Days | Contracts over $50,000 will require Manager or Director Review. |
| Customer Meetings | N/A | SA or TAM will provide context up front of to the Customer or Prospects questions or concerns prior to the meeting.<br/>Field Security will provide a PowerPoint presentation with critical information about GitLab Security and specifics to the Customer or Prospect's request.<br/>If the opportunity is greater than or equal to $250,000, the VP of Security will also attend. |
| External Evidence| 2 Business Days | SA or TAM must provide the name and email address of the recipient. |

## Exceptions
If the Account Owner or Customer Success point of contact feel they have sufficient knowledge and resoures to complete a Customer Assessment, this procedure does not have to used. These excpetions, will not be tracked. 

## References
Not applicable
