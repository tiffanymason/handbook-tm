---
layout: handbook-page-toc
title: "Security Assurance"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
As a member of the [Engineering](/handbook/engineering/) organization and sub-department of the greater [Security department](/handbook/engineering/security/#assure-the-customer), the Security Assurance team provides GitLab customers with a high level of assurance around the security of GitLab as an enterprise application.

## <i class="fab fa-gitlab fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i> Functions of Security Assurance

There are four functions and three teams in the Security Assurance sub department:
* [Field Security](/handbook/engineering/security/security-assurance/field-security/) and [Security Governance](/handbook/engineering/security/security-assurance/governance/)
* [Security Compliance](/handbook/engineering/security/security-assurance/security-compliance/compliance.html)
* [Security Risk](/handbook/engineering/security/security-assurance/security-risk/)

## <i id="biz-tech-icons" class="fas fa-tasks"></i>Security Assurance Sub-Department

<table id="core-competencies">
  <tr>
    <th>
        <i class="fas fa-hands-helping i-bt"></i>
        <h5>Field Security</h5>
    </th>
    <th>
        <i class="fas fa-bullseye i-bt"></i>
        <h5>Security Compliance</h5>
    </th>
    <th>
        <i class="fas fa-users-cog i-bt"></i>
        <h5>Security Governance</h5>
    </th>
    <th>
        <i class="fas fa-shield-alt i-bt"></i>
        <h5>Security Risk</h5>
    </th>
  </tr>
  <tr>
      <td>
        <ul>
            <li>Security Sales Enablement</li>
            <li>Customer Assurance Documentation</li>
            <li>Security Customer Support</li>
        </ul>
      </td>
      <td>
        <ul>
            <li>Continuous Control Monitoring/Auditing</li>
            <li>GitLab SOX ITGC Compliance</li>
            <li>Security Certifications</li>
            <li>Tier 3 Observation Management</li>
        </ul>
      </td>
      <td>
        <ul>    
            <li>Security Policies, Standards and Control maintenance </li>
            <li>Security Assurance Metrics</li>
            <li>Security Awareness and Training</li>
            <li>Regulatory Landscape Monitoring </li>
            <li>Security Assurance Application Administration </li>
        </ul>
      </td>
      <td>
        <ul>
            <li>Third Party Risk Management</li>
            <li>Tier 2 Security Operational Risk Management</li>          
        </ul>
      </td>
  </tr>
</table>

## <i id="biz-tech-icons" class="far fa-newspaper"></i>Core Tools and Systems

### Field Security Core Competencies 
These are the primary functions of the Field Security team:
* [Sales Training (Security)](/handbook/sales/onboarding/sqs-learning-objectives/)
* [Sales Enablement (Security)](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/answerbase.html)
* [Customer Knowledge Management (Security)](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html)
* [Customer Support (Security)](/handbook/engineering/security/#external-contact-information)

### Security Governance Core Competencies 
* Security Policies, Standards and Control maintenance 
* Security Assurance Metrics
* [Security Awareness and Training](/handbook/engineering/security/security-assurance/governance/sec-awareness-training.html)
* Regulatory Landscape Monitoring 
* Security Assurance Application Administration 

### Security Risk Core Competencies 
* [Third Party Risk Management](/handbook/engineering/security/security-assurance/security-risk/third-party-risk-management.html)
* [Tier 2 Operational Security Risk Management](/handbook/engineering/security/security-assurance/security-risk/storm-program/index.html)

### Security Compliance Core Competencies 
* [Continuous Control Monitoring/Auditing](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
* [GitLab SOX ITGC Compliance](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/ITGC/)
* [Security Certifications](/handbook/engineering/security/security-assurance/security-compliance/certifications.html)
* [Tier 3 Observation Management](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html)

## Core Tools and Systems
The Security Assurance sub department utilizes variety of tools and systems to carry out day to day activities related to the core competencies metnioned above. Some key tools that are utilized:
* [ZenGRC](/handbook/business-ops/tech-stack/#zengrc): Key system utilized for initiating, tracking/documenting, and completing Governance, Risk, and Compliance related activities. For details on the various processes that are carried out of ZenGRC, refer to the [ZenGRC Activities](/handbook/engineering/security/security-assurance/zg-activities.html) handbook page.
* [GitLab](/handbook/business-ops/tech-stack/#gitlab): Primarily used to engage stakeholders via issues, updates to Security Assurance related handbook pages, etc.
* [Slack](/handbook/business-ops/tech-stack/#slack) for internal communication
* [BitSight](/handbook/engineering/security/security-assurance/field-security/independent_security_assurance.html): Third Party Security Rating Platform used to monitor GitLab's security rating as well as assess our Third Party's ratings. 

## <i id="biz-tech-icons" class="fas fa-users"></i>Contacting the Team

* Join our slack channel: #sec-assurance
* Email: <security-assurance@gitlab.com>

## Customer Resources

Check out these great security resources built with our customers in mind: 

* GitLab's [Customer Assurance Package](https://about.gitlab.com/security/cap/)
* GitLab's [Security - Trust Center](https://about.gitlab.com/security/)
* GitLab's [Security Team Page](/handbook/engineering/security/) 
* [Security Terms Glossary](/handbook/engineering/security/security-assurance/security-terms-glossary.html)
