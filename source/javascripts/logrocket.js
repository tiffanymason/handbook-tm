import LogRocket from 'logrocket';

(function () {
    // Only initialize LogRocket in production - we technically run Middleman and Webpack in "production" mode for review apps,
    // so this will check if the script is running on a site with the `about.gitlab.com` domain. That should skip review apps appropriately.
    if (window.location.hostname.indexOf('about.gitlab.com') !== -1) {
        initializeLogRocket();
    }
})()

function initializeLogRocket() {
    LogRocket.init('9jygng/gitlab', {
        dom: {
            inputSanitizer: true,
        }
    });
    LogRocket.getSessionURL(function (sessionURL) {
        ga('send', {
            hitType: 'event',
            eventCategory: 'LogRocket',
            eventAction: sessionURL,
        });
        drift.track('LogRocket', { sessionURL: sessionURL });
    });
}
